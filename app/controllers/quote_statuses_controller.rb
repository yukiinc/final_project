class QuoteStatusesController < ApplicationController
  before_action :set_quote_status, only: [:show, :edit, :update, :destroy]

  # GET /quote_statuses
  # GET /quote_statuses.json
  def index
    @quote_statuses = QuoteStatus.all
  end

  # GET /quote_statuses/1
  # GET /quote_statuses/1.json
  def show
  end

  # GET /quote_statuses/new
  def new
    @quote_status = QuoteStatus.new
  end

  # GET /quote_statuses/1/edit
  def edit
  end

  # POST /quote_statuses
  # POST /quote_statuses.json
  def create
    @quote_status = QuoteStatus.new(quote_status_params)

    respond_to do |format|
      if @quote_status.save
        format.html { redirect_to @quote_status, notice: 'Quote status was successfully created.' }
        format.json { render :show, status: :created, location: @quote_status }
      else
        format.html { render :new }
        format.json { render json: @quote_status.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /quote_statuses/1
  # PATCH/PUT /quote_statuses/1.json
  def update
    respond_to do |format|
      if @quote_status.update(quote_status_params)
        format.html { redirect_to @quote_status, notice: 'Quote status was successfully updated.' }
        format.json { render :show, status: :ok, location: @quote_status }
      else
        format.html { render :edit }
        format.json { render json: @quote_status.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /quote_statuses/1
  # DELETE /quote_statuses/1.json
  def destroy
    @quote_status.destroy
    respond_to do |format|
      format.html { redirect_to quote_statuses_url, notice: 'Quote status was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_quote_status
      @quote_status = QuoteStatus.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def quote_status_params
      params.require(:quote_status).permit(:status_type)
    end
end
