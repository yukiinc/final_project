class SalesStatusesController < ApplicationController
  before_action :set_sales_status, only: [:show, :edit, :update, :destroy]

  # GET /sales_statuses
  # GET /sales_statuses.json
  def index
    @sales_statuses = SalesStatus.all
  end

  # GET /sales_statuses/1
  # GET /sales_statuses/1.json
  def show
  end

  # GET /sales_statuses/new
  def new
    @sales_status = SalesStatus.new
  end

  # GET /sales_statuses/1/edit
  def edit
  end

  # POST /sales_statuses
  # POST /sales_statuses.json
  def create
    @sales_status = SalesStatus.new(sales_status_params)

    respond_to do |format|
      if @sales_status.save
        format.html { redirect_to @sales_status, notice: 'Sales status was successfully created.' }
        format.json { render :show, status: :created, location: @sales_status }
      else
        format.html { render :new }
        format.json { render json: @sales_status.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /sales_statuses/1
  # PATCH/PUT /sales_statuses/1.json
  def update
    respond_to do |format|
      if @sales_status.update(sales_status_params)
        format.html { redirect_to @sales_status, notice: 'Sales status was successfully updated.' }
        format.json { render :show, status: :ok, location: @sales_status }
      else
        format.html { render :edit }
        format.json { render json: @sales_status.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /sales_statuses/1
  # DELETE /sales_statuses/1.json
  def destroy
    @sales_status.destroy
    respond_to do |format|
      format.html { redirect_to sales_statuses_url, notice: 'Sales status was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_sales_status
      @sales_status = SalesStatus.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def sales_status_params
      params.require(:sales_status).permit(:status_type)
    end
end
