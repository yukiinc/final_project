json.array!(@cars) do |car|
  json.extract! car, :id, :make_id, :year_id, :color_id, :vin, :price
  json.url car_url(car, format: :json)
end
