json.array!(@employees) do |employee|
  json.extract! employee, :id, :name, :email, :address, :number, :emp_position_id
  json.url employee_url(employee, format: :json)
end
