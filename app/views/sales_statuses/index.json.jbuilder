json.array!(@sales_statuses) do |sales_status|
  json.extract! sales_status, :id, :status_type
  json.url sales_status_url(sales_status, format: :json)
end
