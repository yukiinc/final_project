json.array!(@customers) do |customer|
  json.extract! customer, :id, :name, :number, :address
  json.url customer_url(customer, format: :json)
end
