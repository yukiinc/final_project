json.array!(@quote_statuses) do |quote_status|
  json.extract! quote_status, :id, :status_type
  json.url quote_status_url(quote_status, format: :json)
end
