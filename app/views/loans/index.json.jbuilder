json.array!(@loans) do |loan|
  json.extract! loan, :id, :quote_id, :terms, :interest_rate
  json.url loan_url(loan, format: :json)
end
