json.array!(@quotes) do |quote|
  json.extract! quote, :id, :quote_price, :quote_status_id, :car_id
  json.url quote_url(quote, format: :json)
end
