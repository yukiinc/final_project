class Car < ActiveRecord::Base
  has_many :quotes
  belongs_to :color
  belongs_to :make
  belongs_to :year

  def getModel
    "#{make.make_name}"
  end
end
