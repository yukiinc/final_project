class Sale < ActiveRecord::Base
  belongs_to :quote
  belongs_to :sales_status, :foreign_key => "sale_status_id"
  belongs_to :employee
  belongs_to :customer


  def quotetax
    w = 0.1
    t = 0.043
    q = quote.quote_price
    e = ((q*w) + q)
    total = (e + (e * t))
    return total
  end
end
