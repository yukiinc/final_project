class CreateEmployees < ActiveRecord::Migration
  def change
    create_table :employees do |t|
      t.string :name
      t.string :email
      t.string :address
      t.integer :number
      t.integer :emp_position_id

      t.timestamps null: false
    end
  end
end
