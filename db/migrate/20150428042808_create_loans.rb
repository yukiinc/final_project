class CreateLoans < ActiveRecord::Migration
  def change
    create_table :loans do |t|
      t.integer :quote_id
      t.integer :terms
      t.decimal :interest_rate

      t.timestamps null: false
    end
  end
end
