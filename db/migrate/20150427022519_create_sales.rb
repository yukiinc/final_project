class CreateSales < ActiveRecord::Migration
  def change
    create_table :sales do |t|
      t.integer :quote_id
      t.integer :customer_id
      t.integer :employee_id
      t.integer :sale_status_id
      t.date :date
      t.decimal :total

      t.timestamps null: false
    end
  end
end
