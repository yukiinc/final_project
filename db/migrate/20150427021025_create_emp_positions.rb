class CreateEmpPositions < ActiveRecord::Migration
  def change
    create_table :emp_positions do |t|
      t.string :position_desc

      t.timestamps null: false
    end
  end
end
