class CreateQuotes < ActiveRecord::Migration
  def change
    create_table :quotes do |t|
      t.decimal :quote_price
      t.integer :quote_status_id
      t.integer :car_id

      t.timestamps null: false
    end
  end
end
