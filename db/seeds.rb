# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

Customer.create(name: 'Bob Builder', number: '',address: '18856 Quintero Dr')
Customer.create(name: 'James Bond', number: '',address: '78898 Silent Hill Dr')
Customer.create(name: 'Jesses James', number: '',address: '66666 Hevansward Dr')

Color.create(color_name: 'orange')
Color.create(color_name: 'blue')
Color.create(color_name: 'yellow')

Make.create(make_name: 'Toyota Rava4')
Make.create(make_name: 'Kia Siris')
Make.create(make_name: 'Honda Accorrd')

Year.create(year_date: '1999')
Year.create(year_date: '2015')
Year.create(year_date: '2016')

Quote.create(quote_name: 'ABC2', quote_price: 23000.00,quote_status_id: 1,car_id: 1 )
Quote.create(quote_name: 'ABC1', quote_price: 18000.00,quote_status_id: 1,car_id: 2 )
Quote.create(quote_name: 'ABC3', quote_price: 15000.00,quote_status_id: 2,car_id: 3 )

QuoteStatus.create(status_type: 'Active')
QuoteStatus.create(status_type: 'Inactive')

Sale.create(quote_id: 1, customer_id: 1, employee_id: 1, sale_status_id: 1)
Sale.create(quote_id: 1, customer_id: 2, employee_id: 2, sale_status_id: 2)
Sale.create(quote_id: 1, customer_id: 3, employee_id: 1, sale_status_id: 1)

SalesStatus.create(status_type: 'Active')
SalesStatus.create(status_type: 'Inactive')

EmpPosition.create(position_desc: 'Manager')
EmpPosition.create(position_desc: 'Sales')

Employee.create(name: 'David Bowl', email:'123@gmail.com', address:'79894 rode rd', emp_position_id: 1)
Employee.create(name: 'Betty White', email:'654654@gmail.com', address:'1231 basil rd', emp_position_id: 2)
Employee.create(name: 'Josh Bai', email:'787897@gmail.com', address:'7555 rose rd', emp_position_id: 1)

Car.create(make_id:1, year_id: 1, color_id: 1, vin: 'as12322', price: 18000.00)
Car.create(make_id:2, year_id: 2, color_id: 2, vin: '456dsad', price: 20000.00)
Car.create(make_id:3, year_id: 3, color_id: 3, vin: 'dsad66', price: 15000.00)

Loan.create(quote_id:1, terms:3, interest_rate: 2.3)
Loan.create(quote_id:2, terms:4, interest_rate: 5.25)
Loan.create(quote_id:3, terms:5, interest_rate: 4.5)