require 'test_helper'

class EmpPositionsControllerTest < ActionController::TestCase
  setup do
    @emp_position = emp_positions(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:emp_positions)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create emp_position" do
    assert_difference('EmpPosition.count') do
      post :create, emp_position: { position_desc: @emp_position.position_desc }
    end

    assert_redirected_to emp_position_path(assigns(:emp_position))
  end

  test "should show emp_position" do
    get :show, id: @emp_position
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @emp_position
    assert_response :success
  end

  test "should update emp_position" do
    patch :update, id: @emp_position, emp_position: { position_desc: @emp_position.position_desc }
    assert_redirected_to emp_position_path(assigns(:emp_position))
  end

  test "should destroy emp_position" do
    assert_difference('EmpPosition.count', -1) do
      delete :destroy, id: @emp_position
    end

    assert_redirected_to emp_positions_path
  end
end
