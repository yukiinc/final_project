require 'test_helper'

class QuoteStatusesControllerTest < ActionController::TestCase
  setup do
    @quote_status = quote_statuses(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:quote_statuses)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create quote_status" do
    assert_difference('QuoteStatus.count') do
      post :create, quote_status: { status_type: @quote_status.status_type }
    end

    assert_redirected_to quote_status_path(assigns(:quote_status))
  end

  test "should show quote_status" do
    get :show, id: @quote_status
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @quote_status
    assert_response :success
  end

  test "should update quote_status" do
    patch :update, id: @quote_status, quote_status: { status_type: @quote_status.status_type }
    assert_redirected_to quote_status_path(assigns(:quote_status))
  end

  test "should destroy quote_status" do
    assert_difference('QuoteStatus.count', -1) do
      delete :destroy, id: @quote_status
    end

    assert_redirected_to quote_statuses_path
  end
end
